angular.module('starter.controllers', [])
.controller('ToDoListControl', function($scope, $ionicModal) {
	// Create the login modal that we will use later
	var loveModal = $ionicModal.fromTemplateUrl('views/loveModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.loveModal = modal;
	});
	
	var categoryCreateModal = $ionicModal.fromTemplateUrl('views/categoryModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.categoryModal = modal;
	});
	
	
	console.log('loveModal modal = ', loveModal);
	console.log('categoryCreateModal modal = ', categoryCreateModal);


	$scope.categories = [
	    { title: 'Outings', id: 1 },
	    { title: 'Shopping', id: 2 },
	    { title: 'Watch (movies and tv)', id: 3 },
	    { title: 'DIW', id: 4 },
	    { title: 'The Simple Things', id: 5 },
	    { title: 'Explore Each Other', id: 6 },
	    { title: 'Outings', id: 1 },
	    { title: 'Shopping', id: 2 },
	    { title: 'Watch (movies and tv)', id: 3 },
	    { title: 'DIW', id: 4 },
	    { title: 'The Simple Things', id: 5 },
	    { title: 'Explore Each Other', id: 6 },
	    { title: 'Outings', id: 1 },
	    { title: 'Shopping', id: 2 },
	    { title: 'Watch (movies and tv)', id: 3 },
	    { title: 'DIW', id: 4 },
	    { title: 'The Simple Things', id: 5 },
	    { title: 'Explore Each Other', id: 6 }
  ];
	
	$scope.openLoveModal = function() {
  		$scope.loveModal.show();
	};
	
	$scope.closeLoveModal = function() {
		$scope.loveModal.hide();
	};
	
	$scope.openCategoryModal = function() {
		alert('the category modal should have been created');
		$scope.categoryModal.show();
	};
	
	$scope.closeCategory = function() {
		$scope.categoryModal.hide();
	};
	
	$scope.createCategory = function() {
		alert('category will be created');
		$scope.categoryModal.hide();
	};
	
	$scope.closeMainMenu = function() {
		var thisMenu = document.getElementById("mainMenu");
		console.log('main menu is ', thisMenu);
		TweenLite.to(thisMenu, .5, {x:300, alpha:0});
		
		var heartIcon = document.getElementById("heartIconHolder");
		TweenLite.to(heartIcon, .5, {y:0, alpha:100});
	}
	
	$scope.itemClicked = function(whichItem) {
		console.log('an item has been clicked - ' +whichItem);
		//add in the tween lite here:
		var thisMenu = document.getElementById("mainMenu");
		console.log('main menu is ', thisMenu);
		TweenLite.to(thisMenu, .5, {x:300, alpha:0});
		
		var heartIcon = document.getElementById("heartIconHolder");
		TweenLite.to(heartIcon, .5, {y:0, alpha:100});
		
		// reset the scroll to 0
	};
	
	$scope.heartClick = function() {
		var thisMenu = $("#mainMenu");
		thisMenu.show();
		console.log('main menu is ', thisMenu);
		TweenLite.to(thisMenu, 0, {x:300, alpha:100});
		TweenLite.to(thisMenu, .5, {x:0, alpha:100});
		
		var heartIcon = document.getElementById("heartIconHolder");
		TweenLite.to(heartIcon, 1, {y:800, alpha:0});
		
		// reset the scroll to 0
	};
});