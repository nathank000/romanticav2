romantisca.service('commentService', ['$http', function($http) {
	var commentService = {
		comments:[
			{title: 'this is comment one', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 2', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 3', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 4', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 5', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 6', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 7', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 8', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 9', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 10', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 11', item:2, content: 'this is the content of the comment'},
			{title: 'this is comment 12', item:2, content: 'this is the content of the comment'}
		],
		
		init: function() {
			return $http.get(commentService.commentsEndpoint)
			.then(function(response) {
				console.log('CATEGORY RESPONSE::: ', response);
				if (typeof response.data === 'object') {
					commentService.comments = response.data.data;
					return commentService.comments;
				}
				else {
					return $q.reject(response.data);
				}
			}, function () {
				return $q.reject(response.data);
			});
		},
		
		
	};
	
	return commentService;
}]);
