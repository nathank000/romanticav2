romantisca.service('categoryService', ['$http', '$q', function($http, $q) {
	var catService =  {
		localCategoryEndpoint: 'http://127.0.0.1/romantisca/api/categories.php?action=read&group',
		categoryEndpoint: 'http://romantica.io/api/categories.php?action=read&group=',
		
        allIcons: [
            {iconTitle: 'Cash', iconClass: 'ion-cash'},
            {iconTitle: 'Pricetag', iconClass: 'ion-pricetag'},
            {iconTitle: 'Compass', iconClass: 'ion-compass'},
            {iconTitle: 'Fork', iconClass: 'ion-fork'},
            {iconTitle: 'Monitor', iconClass: 'ion-monitor'},
            {iconTitle: 'Flame', iconClass: 'ion-flame'},
            {iconTitle: 'Chat', iconClass: 'ion-chatboxes'},
            {iconTitle: 'Friend', iconClass: 'ion-person-stalker'}
        ],
		// categories:[
		    // { title: 'Outings', icon:'ion-compass', image:'novi-sad-fortress.jpg', id: 0 },
		    // { title: 'Shopping', icon:'ion-pricetag', image:'shopping.jpg', id: 1 },
		    // { title: 'Food and Drink', icon:'ion-fork', image:'food-and-drinks.jpg', id: 2 },
		    // { title: 'Watch (movies and tv)', icon:'ion-monitor', image:'watching.jpg', id: 3 },
		    // { title: 'DIW', icon:'ion-flame', image:'diw.jpg', id: 4 },
		    // { title: 'The Simple Things', icon:'ion-chatboxes', image:'simple_things.jpeg', id: 5 },
		    // { title: 'Explore Each Other', icon:'ion-person-stalker', image:'getting_to_know.jpg', id: 6 }
  		// ],
		
		init: function(location, group) {
			if (!group) {
				
				console.log('CATEGORY SERVICE::: no group passed');
			}
			
			if ((location != undefined) && (location == 'local')) {
				endpoint = catService.localCategoryEndpoint;
			}
			else {
				endpoint = catService.categoryEndpoint;
			}
			
			endpoint += group;
            console.log('CATEGORY RESPONSE::: endpoint = ' + endpoint);
			return $http.get(endpoint)
			.then(function(response) {
				console.log('CATEGORY RESPONSE::: ', response);
				if (typeof response.data === 'object') {
					catService.categories = response.data.data;
					return catService.categories;
				}
				else {
					return $q.reject(response.data);
				}
			}, function() {
				return $q.reject(response.data);
			});
		},
		
		getAllCategories: function() {
			console.log('CATEGORY_SERVICE::: returning all categories: ', catService.categories);
			return catService.categories;
		},
		
		getCategory: function(whichCategory) {
			for(var i=0; i<catService.categories.length; i++) {
				console.log('checking category: ' ,catService.categories[i]);
				if (catService.categories[i].id == whichCategory) {
					console.log('CATEGORY REQUESTED = ' + whichCategory + ' this ID = ',catService.categories[i].id);
					return catService.categories[i];
				}
			}
			return 'no category found for that id';
		},
		
		getCategroryTitle: function(whichCategory) {
			for(var i=0; i<catService.categories.length; i++) {
				if (catService.categories[i].id == whichCategory) {
					return catService.categories[i].title;
				}
			}
			return 'no category found for that id';
		},
		
		getCategroryIcon: function(whichCategory) {
			for(var i=0; i<catService.categories.length; i++) {
				if (catService.categories[i].id == whichCategory) {
					return catService.categories[i].icon;
				}
			}
			return 'no category found for that id';
		},
        
        getAvailableIcons: function() {
            return catService.allIcons;
        }
	};
	
	return catService;
}]);