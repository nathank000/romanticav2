//the point of this file is to grab the user and their group details from the server and preferrably save it to the
//local machine for later use
romantisca.service('userAndGroupService', ['$http', '$state', function($http, $state){
	var usrGroupService = {
		
		localUserEndpoint: 'http://127.0.0.1/romantisca/api',
		remoteUserEndpoint: 'http://romantica.io/api',
		userData: 'noUser',
		location: '',
		
		init: function(location) {
			//console.log('user service yo - initted');
			//check to see if this is stored on the machine -
			//if it is not, then we should fetch it.
			//if it is, then just return that data
			
			usrGroupService.location = location;
		},
		
		setVal: function(key, val) {
			console.log('USER_SERVICE::: yo - SETTING the key ' +key +'to the value ', val);
			window.localStorage.setItem(key, JSON.stringify(val));
		},
		
		getVal: function(key) {
			var val = JSON.parse(window.localStorage.getItem(key));
			console.log('USER_SERVICE::: yo - GETTING the key ' +key +'with the value ', val);
			return val;
		},
		
		verifyUser: function() {
			
			console.log('USER_SERVICE::: verifying user');
			
			//check to see if we have access to the users details
			var thisUser = usrGroupService.getVal('user');
			if (!thisUser) {
		
				console.log('USER_SERVICE::: no user credentials, sending the user to login');
				//on submit call for the fetching of the user using the authenticate method
				$state.go('userLogin');				
			}
			else {
				console.log('USER_SERVICE::: user found: ', thisUser);
				return thisUser;
			}
			
		},
		
		
		CLEAR: function() {
			//unset the user for logout and testing
			window.localStorage.setItem('user', null);
			usrGroupService.userData = 'noUser';
			console.log('USER_SERVICE::: the user should have been cleared');
			$state.go('userLogin');
		},
		
		authenticate: function(username, password) {
			//todo: how do we encrypt for sending over the wire? 
			
			//check to see if we already have a user logged in
			if (usrGroupService.userData != 'noUser') {
				console.log('we already have a user: ', usrGroupService.userData);
				return usrGroupService.userData;
			}
			
			
			console.log('USER_SERVICE::: username = ' +username);
			console.log('USER_SERVICE::: password = ' +password);
			
			if ((usrGroupService.location != undefined) && (usrGroupService.location == 'local')) {
				endpoint = usrGroupService.localUserEndpoint + '/users.php?action=login';
				console.log('USER_SERVICE::: GOING LOCAL! making a call to ' +endpoint);
			}
			else {
				endpoint = usrGroupService.remoteUserEndpoint + '/users.php?action=login';
			}
			
			//this should be an http post
			return $http.post(endpoint, {postUsername:username, postPassword: password})
			///////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////
			//sweet jesus
			//http://stackoverflow.com/questions/19254029/angularjs-http-post-does-not-send-data
			/////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////
			.then(function(response) {
				console.log('USERGROUPSERVICE RESPONSE::: ', response);
				console.log('USERGROUPSERVICE RESPONSE.DATA.DATA[0]::: ', response.data.data[0]);
				if (typeof response.data === 'object') {

					if (response.data.data[0] != undefined) {
						usrGroupService.userData = response.data.data[0];
					
						console.log('USERGROUPSERVICE SERVICE::: response.data.data = ', response.data.data[0]);
						
						for (var foo in usrGroupService.userData) {
							console.log('USERGROUPSERVICE SERVICE DUMP::: var ' + foo + ' = ' +usrGroupService.userData[foo]);
						}
						
						console.log('USERGROUPSERVICE SERVICE::: usrGroupService.userData = ', usrGroupService.userData);
						//permenantly store th user ID and group ID to the local machine
						//that way if it is not there we know to post up the login screen
						usrGroupService.setVal('user', usrGroupService.userData);
						console.log('USERGROUPSERVICE SERVICE::: sending the user home ');
						$state.go('home');
						return usrGroupService.userData;	
					}
					else {
						//HANDLE ERRORS
						console.log('USERGROUPSERVICE RESPONSE [fail]::: returning no_user');
						console.log('USERGROUPSERVICE RESPONSE [fail]::: sending the user to the incorrectLogin page');
						$state.go('incorrectLogin');
						return 'no_user';
						console.log('USERGROUPSERVICE RESPONSE [fail]::: AFTER RETURN');
					}
				}
				else {
					return $q.reject(response.data);
				}
			}, function() {
				return $q.reject(response.data);
			});
			
		}
		
	};
	
	return usrGroupService;
}]);
