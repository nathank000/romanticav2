romantisca.service('categoryItemService', ['$http', function($http){
	var catItemService = {
		localItemEndpoint: 'http://127.0.0.1/romantisca/api/toDoItems.php?action=read',
		catItemsEndpoint: 'http://romantica.io/api/toDoItems.php?action=read&group=',
		//localItemImagePrefix: 'http://127.0.0.1/romantisca/',
		//prodItemImagePrefix: 'http://romantica.io/user/',
		environment: '',
		endpointPrefix: '',
		
		
		init: function(location, group) {
			if (!group) {
				
				console.log('CATEGORY ITEM SERVICE::: no group passed');
			}
			
			if ((location != undefined) && (location == 'local')) {
				endpoint = catItemService.localItemEndpoint;
				currentLocation = 'local';
				catItemService.endpointPrefix = 'http://127.0.0.1/romantisca/';
			}
			else {
				endpoint = catItemService.catItemsEndpoint;
				currentLocation = 'remote';
				catItemService.endpointPrefix = 'http://romantica.io/';
			}
			
			endpoint += group;
			catItemService.environment = currentLocation; 
			
			return $http.get(endpoint)
			.then(function(response) {
				if (typeof response.data === 'object') {
					catItemService.demoCategoryItems = response.data.data;
					return catItemService.demoCategoryItems;
				}
				else {
					return $q.reject(response.data);
				}
			}, function() {
				return $q.reject(response.data);
			});
		},
		
        
        reloadItems: function(location) {
			
			catItemService.environment = currentLocation; 
			
			return $http.get(endpoint)
			.then(function(response) {
				if (typeof response.data === 'object') {
					catItemService.demoCategoryItems = response.data.data;
					return catItemService.demoCategoryItems;
				}
				else {
					return $q.reject(response.data);
				}
			}, function() {
				return $q.reject(response.data);
			});
		},
        
        
		getAllItems: function(){
			return catItemService.demoCategoryItems;
		},
		
		getCategoryItems: function(whichCategory) {
			var thisCategoryItems = [];
			for(var i=0; i<catItemService.demoCategoryItems.length; i++) {
				if (catItemService.demoCategoryItems[i].category == whichCategory) {
					console.log('ADDING ITEM ', catItemService.demoCategoryItems[i]);
					thisCategoryItems.push(catItemService.demoCategoryItems[i]);
					//handle image prefix here
				}
			}
			return thisCategoryItems;
		},
		
		getItem: function(itemId) {
			for(var i=0; i<catItemService.demoCategoryItems.length; i++) {
				if (catItemService.demoCategoryItems[i].id == itemId) {
					console.log('RETURNING ITEM ', catItemService.demoCategoryItems[i], 'with ID: ', itemId);
					return catItemService.demoCategoryItems[i];
				}
			}
			return 'no item found for this id';
		},
		
		getRandomCategoryItem: function(whichCategory) {
			console.log('fetching for cat' +whichCategory);
			
			var tempItems = angular.copy(catItemService.demoCategoryItems);
			console.log('tempItems = ', tempItems);
			
			tempItems.sort(function() { return 0.5 - Math.random(); });
			
			for(var i=0; i<=tempItems.length; i++) {
				console.log('looking at tempItem = ', tempItems[i]);
				console.log('tempItems[i].category = ', tempItems[i].category);
				console.log('whichCategory = ', whichCategory);
				
				if (tempItems[i].category == whichCategory) {
					console.log('returning tempItems = ', tempItems[i]);
					return tempItems[i];
				}
			}
		},
		
		getRandomItem: function() {
			var randNum = Math.floor(Math.random()*catItemService.demoCategoryItems.length);
			return catItemService.demoCategoryItems[randNum];	
		},
		
		markItemDone: function(whichItem) {
			//send a note to the endpoint that an item is complete
			var endpoint = catItemService.endpointPrefix + 'api/toDoItems.php?action=markAsDone&id='+whichItem;
			$http.get(endpoint);
			console.log('calling get on the endpoint: ' + endpoint);
		},
		likeit: function(whichItem,lby) {
			//send a note to the endpoint that an item is complete
			var endpoint = catItemService.endpointPrefix + 'api/toDoItems.php?action=update&id='+whichItem+'&likedby='+lby;
			$http.get(endpoint);
			console.log('calling get on the endpoint: ' + endpoint);
		},
		deleteItem: function(whichItem) {
			var endpoint = catItemService.endpointPrefix + 'api/toDoItems.php?action=delete&id='+whichItem;
			$http.get(endpoint);
			console.log('calling get on the endpoint: ' + endpoint);
		}
	};
	
	return catItemService;
}]);
