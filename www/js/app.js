// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var romantisca = angular.module('romantisca', ['ionic', 'starter.controllers']);

romantisca.run(function($ionicPlatform) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
      if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.hide();
    }
  });
});/*
romantisca.filter('firstLetterFilter', [function () {
 return function (items) {
    var firstLetters = [];
    items.forEach(function (item1) {
      var firstLetter = item1.likedBy.charAt(0);
      if (firstLetters.indexOf(firstLetter) === -1) {
        firstLetters.push(firstLetter);
      }
    });
    return firstLetters;
  }
}]);*/
romantisca.config(function($stateProvider, $urlRouterProvider) {
	
	$stateProvider.state('home', {
		cache: false, //this keeps us from having to reset the state to it's original view by not caching the view
		url: '/', 
		templateUrl: "views/main.html"
	})
	
	.state('incorrectLogin', {
		controller: 'noUser',  //is there a need for a custom controller
		url: '/noUser', 
		templateUrl: "views/incorrectLogin.html"
	})
	
	.state('categoriesList', {
		cache: false,
		controller: 'categoriesController',
		url: '/categoriesList', 
		templateUrl: "views/categoriesList.html"
	})
	
	.state('itemsListByCategory', {
		cache: false,
		controller: 'todoItemsController',
		url: '/itemsListByCategory/:categoryID', 
		templateUrl: "views/itemsList.html"
	})
	
	.state('itemsList', {
		cache: false,
		controller: 'todoItemsController',
		url: '/items', 
		templateUrl: "views/itemsList.html"
	})
	
	.state('itemDetail', {
		cache: false,
		controller: 'itemDetailController',
		url: '/item/:itemID', 
		templateUrl: "views/itemDetail.html"
	})
	
	.state('userLogin', {
		controller: 'userLoginController',
		url: '/user/login', 
		templateUrl: 'views/userLogin.html'
	});
	
	$urlRouterProvider.otherwise('/');
});

romantisca.directive("appSideMenu", function() {
    return {
        templateUrl: 'js/directives/leftMenu.html'
    }
});