//var serviceAPILocation = 'local';
var serviceAPILocation = 'remote';

angular.module('starter.controllers', [])
//fetching content from the camera
//http://metabates.com/2012/04/15/capturing-and-uploading-photos-on-ios-with-phonegap/
.controller('ToDoListControl', [
								'$scope',
								'$rootScope',
								'$ionicModal',
								'$ionicLoading',
								'$location',
								'$state',
                                '$http',
								'$timeout',
								'categoryService',
								'categoryItemService',
								'userAndGroupService',
								'$ionicPopup',
								function($scope, $rootScope, $ionicModal, $ionicLoading, $location, $state, $http, $timeout, categoryService, categoryItemService, userAndGroupService,$ionicPopup) {
									
									
	console.log('todo list contorl is loaded');
	//create a new todoItem to 
	$scope.todoItem = {};
    $scope.newCategory = {};
    $scope.newItemType = 'todo';
	$scope.userAuthenticated = true;       
	
	//check for the user either from the server or the 

	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('views/loginModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.loginModal = modal;
	});
	
	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('views/loveModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.loveModal = modal;
	});
	
	//Creation of the category modal
	$ionicModal.fromTemplateUrl('views/categoryModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.categoryModal = modal;
	});
	
	//Creation of the new item modal
	$ionicModal.fromTemplateUrl('views/newItemModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.newItemModal = modal;
	});
	
	
	//Creation of the loading modal
	$scope.showLoading = function(loaderText, loadDuration) {
		if (loadDuration === 'undefined') { loadDuration = 0; }
		$ionicLoading.show({
			//templateUrl: 'views/loaderTemplate.html',
			template: "<ion-spinner icon='ripple'></ion-spinner>" +loaderText,
			duration: loadDuration
		});
	};
	
	
	$scope.hideLoading = function() {
		$ionicLoading.hide();
	};
	
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	//working with authentication test
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	userAndGroupService.init();
	//userAndGroupService.CLEAR();
	//userAndGroupService.setVal('user', {'firstName':'Nathan', 'lastName':'King', 'userId':'0', 'groupId':'0', 'groupName':'nathan&sush'});
	
	//the service sends the user to the userLogin state if the user is not stored
	$scope.user = userAndGroupService.verifyUser();
	console.log('MAIN CONTROLLER USER::: = ' + JSON.stringify($scope.user));
	
	if($scope.user == 'no_user') {
		//throw an errore up
		$scope.showLoading('No user found', 1500);
		//userAndGroupService.CLEAR();
	}
	
	if (($scope.user != undefined)) {
		console.log('CATEGORY FETCHING::: we have gotten a user and are looking for their categories');
		
		//load the categories
		categoryService.init(serviceAPILocation, $scope.user.groupId)
		.then(function(data) {
			if (data) {
				$scope.categories = categoryService.getAllCategories(serviceAPILocation, $scope.user.groupId);
				console.log('CATEGORY SERVICE::: categories have been fetched:', $scope.categories);
			}
		});
		
		
        $scope.availableIcons = categoryService.getAvailableIcons();
        console.log('AVAILABLE ICONS:::', $scope.availableIcons);
        $scope.logNewCat = function() {
            console.log('newCategory = ', $scope.newCategory);
        }
        
		/*
		 * ramdom item loader for the initial page - handles the main 
		 */
		categoryItemService.init(serviceAPILocation, $scope.user.groupId)
		.then(function(data) {
			if (data) {
				$scope.randomSuggestion1 = categoryItemService.getRandomItem();
				$scope.randomSuggestion2 = categoryItemService.getRandomItem();
				$scope.randomSuggestion3 = categoryItemService.getRandomItem();
				$scope.randomSuggestion4 = categoryItemService.getRandomItem();
				$scope.randomSuggestion5 = categoryItemService.getRandomItem();
				$scope.randomSuggestion6 = categoryItemService.getRandomItem();
				$scope.randomSuggestion7 = categoryItemService.getRandomItem();
				$scope.randomSuggestion8 = categoryItemService.getRandomItem();
				$scope.randomSuggestion9 = categoryItemService.getRandomItem();
				$scope.randomSuggestion10 = categoryItemService.getRandomItem();
				console.log('CATEGORY ITEM SERVICE (random)::: random has been fetched:', $scope.randomSuggestion);			
			}
		});
	}
	else {
		console.log('CATEGORY FETCHING::: user is undefined - not looking for categories at this time');
	}
	
	
	
	//for the new item modal we need to handle the upload of the image and the creation of the new item
	//on the server side we need to handle the manipulation of the image
	
	$scope.logout = function() {
		userAndGroupService.CLEAR();	
	};
	
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	//image capture or selection
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	
    //image capture                                
    $scope.captureImage = function(itemType) {
		
		console.log('CAMERA::::', navigator.camera);
		
		navigator.camera.getPicture(onSuccess, onFail, { quality          : 50,
                                                        destinationType   : Camera.DestinationType.FILE_URI
                                                       });
        
        var passedType = itemType;
        //does camera.getPicture allow you to pass additional info that is going to be passed to the success function
        //if not we need to set a global toggle to inidicate what type of new item we are creating.
        
	
		function onSuccess(imageURI) {
		   
            if(itemType == 'category') {
                console.log('IMAGE CAPTURE SUCCESS:: sucessfully selected a category image');
            }
            
		   //button stays to allow the user to take another photo
		   //or the button is gone
		    
		    //show the user a preview of what they selected
		    //var image = document.getElementById('takenImage');
		    //image.src = imageURI;
		    //make sure that we have this
		    $scope.newItemCapturedImage = imageURI;
		    //$scope.todoItem.imageType = 'taken';
            console.log('the passed item type = ' + passedType);
		    console.log('IMAGE CAPTURE::: image = ' +$scope.newItemCapturedImage);
		}
		
		function onFail(message) {
		    alert('Image Capture Failed. Message: ' + message);
		}

	};
	
	//image selection
	$scope.selectImage = function(itemType) {
		
		console.log('CAMERA::::', navigator.camera);
		
		navigator.camera.getPicture(onSuccess, onFail, { quality		: 50,
														sourceType      : Camera.PictureSourceType.SAVEDPHOTOALBUM,
	    												destinationType	: Camera.DestinationType.FILE_URI
                                                       });
	   
        var passedType = itemType;
        
		function onSuccess(imageURI) {
		   
		   //button stays to allow the user to take another photo
		   //or the button is gone
		    
		    //show the user a preview of what they selected
		    //var image = document.getElementById('takenImage');
		    //image.src = imageURI;
		    //make sure that we have this
		    $scope.newItemCapturedImage = imageURI;
		    //$scope.todoItem.imageType = 'selected';
		    
		    console.log('IMAGE SELECT::: image = ' +$scope.newItemCapturedImage);
		}
		
		function onFail(message) {
		    //alert('Failed because: ' + message);
		}

	};
	
	
	/*
	 * method to handle the click of a random suggestion from the front page 
	 */
	$scope.handleSuggestionClick = function(whichItem) {
		console.log('HANDLING A SUGGESTION CLICK, ITEM = ',whichItem);
		$state.go('itemDetail', {itemID:whichItem}, {});
	};
	
	
	/*
	 * method to handle the main screen click of the button "another suggestion"
	 */
	$scope.handleAnotherSuggestionClick = function() {
		console.log('handling another suggestion click');
		//$state.go('home');
        
        if($scope.randomSuggestion == undefined) {
            $scope.randomSuggestion = categoryItemService.getRandomItem();
            $scope.nextSuggestion = categoryItemService.getRandomItem();    
        }
        else {
            $scope.nextSuggestion = $scope.randomSuggestion;
            $scope.randomSuggestion = categoryItemService.getRandomItem();
            
        }
	};
	
	$scope.handleLetsDoItClick = function(whichItem) {
		console.log('handling a DO IT click for the item ' +whichItem);
		
	};
	
	//handling date clicks:
	//http://forum.ionicframework.com/t/date-picker-for-ionic/344/11
	//replaced with HTML5 date and time input fields
	// $scope.showDate = function($event) {
        // var options = {
            // date: new Date(),
            // mode: 'date'
        // };
        // datePicker.show(options, function(date){
            // if(date != 'Invalid Date') {
                // console.log("Date came" + date);
            // } else {
                // console.log(date);
            // }
        // });
        // $event.stopPropagation();  
    // };
	
    ///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	// new category creation function
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	
    $scope.createCategory = function() {
        console.log('creation of a new category with the data that is: ', $scope.newCategory);
        //attach to a new post lobject
        //attach the image to the post object - where are we getting this?
        //upload and post
    };
    
                                    
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	// form handling for the creation of new items
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////

	$scope.postNewItemDetails = function() {
		console.log("validating");
		
		$scope.err = function(err1) {
			
			$ionicPopup.alert({
			 title: 'Alert..!',
			 template: 'Please fill '+"'"+err1+"'!"
			}).then(function(res) {
			 
			});
			
		};
		if($scope.todoItem.title === undefined) {
			$scope.err('What are we doing');
		} else if($scope.todoItem.location === undefined) {
			$scope.err('Location');
		} else if($scope.todoItem.cost === undefined) {
			$scope.err('Cost');
		} else if($scope.todoItem.startDay === null) {
			$scope.err('StartDate');
		} else if($scope.todoItem.startTime === null) {
			$scope.err('StartTime');
		} else if($scope.todoItem.endDay === null) {
			$scope.err('EndDate');
		} else if($scope.todoItem.endTime === null) {
			$scope.err('EndTime');
		} else {
        $scope.showLoading('Creating a new entry');
		
		//working off this:
		//http://metabates.com/2012/04/15/capturing-and-uploading-photos-on-ios-with-phonegap/
		
		console.log('NEW ITEM POST::: $scope.newItemCapturedImage = ', $scope.newItemCapturedImage); 
		//$scope.showLoading('Creating a new item');
		
		  if ($scope.newItemCapturedImage != undefined) {
			console.log('NEW ITEM POST::: todoItem model = ', $scope.todoItem);
              //requires: 
            //cordova plugin add org.apache.cordova.file-transfer
            //https://github.com/apache/cordova-plugin-file-transfer/blob/master/doc/index.md
            options = new FileUploadOptions();

            // parameter name of file:
            options.fileKey = 'newItemFile';

            // name of the file:
            options.fileName = $scope.newItemCapturedImage.substr($scope.newItemCapturedImage.lastIndexOf('/') + 1);  

            // mime type:
            //options.mimeType = "text/plain";  
            
            $scope.todoItem.userGroup = $scope.user.groupId;  
              
            params = $scope.todoItem;

            options.params = params;
            ft = new FileTransfer();
            
            //this should also point to the local server
              
            
            //this should be more generic
            //was: http://romantica.io/api/toDoItems.php?action=create
            if($scope.newItemType == 'todo') {
                
                if (serviceAPILocation == 'local') {
                    postUrl = 'http://127.0.0.1/romantisca/api/toDoItems.php?action=create';
                }
                else {
                    postUrl = 'http://romantica.io/api/toDoItems.php?action=create';
                }
                
                ft.upload($scope.newItemCapturedImage, postUrl, $scope.newItemPostSuccess, newItemPostFail, options);
            } else if ($scope.newItemType == 'category') {
                ft.upload($scope.newItemCapturedImage, postUrl, $scope.newItemPostSuccess, newItemPostFail, options);
            }
            
              
	       }
		  else {
		  	console.log("NEW ITEM POST::: no new image added");
            
            $scope.todoItem.DEFAULT_IMAGE = 'DEFAULT'
            
            if (serviceAPILocation == 'local') {
                serverPrefix = 'http://127.0.0.1/romantisca/api/';
            }
              
                else {
                serverPrefix = 'http://romantica.io/api/';
            }
            
            //based on switch set by modal opening
            //default is todo item
            if($scope.newItemType == 'todo') {
                createUrl =  serverPrefix + 'toDoItems.php?action=create';
                $scope.todoItem.userGroup = $scope.user.groupId;
                params = $scope.todoItem;
                paramData = $.param($scope.todoItem);
                //fill in with data property below
            } else if ($scope.newItemType == 'category') {
                createUrl = serverPrefix +'categories.php?action=create',
                $scope.newCategory.userGroup = $scope.user.groupId;
                params = $scope.newCategory;
                paramData = $.param($scope.newCategory);
            }
              
            //post the form using the http method
            $http({
                method: 'POST',
                //this should be more generic
                //was: http://romantica.io/api/toDoItems.php?action=create
                url: createUrl,
                data: paramData,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
            .success(function(data){
                console.log(data);
                
                if (data.result != 'yes') {
					console.log('data = ', data);
					console.log('data.result = ', data.result);
                    //if not successful, bind errors to scope elements
                    //todo llater
                    $scope.hideLoading();
                    $scope.newItemModal.hide();
                    $scope.showLoading('There was an error creating the new item', 1500);
                    
                }
                else {
                    //handle the success of the post
                    $scope.hideLoading();
                    $scope.newItemModal.hide();
                    $scope.showLoading('New item created', 1500);
                }
            });
		  }
		}
	};
	
	$scope.newItemPostSuccess = function (result) {
		console.log('NEW ITEM POST SUCCESS::: posting was a success');
		console.log('NEW ITEM POST SUCCESS::: something = ', result);
		console.log('NEW ITEM POST SUCCESS::: newItemModal = ', $scope.newItemModal);
        
        ////////////////////////////////////////////////////////////////////
        // testing this - want to be able to see the new items as they are posted 
        // without shutting down the app anc coming back in
        ////////////////////////////////////////////////////////////////////
        console.log('NEW ITEM POST SUCCESS::: calling reload on the category items = ');
        //categoryService.reloadItems();
		
        
        $scope.hideLoading();
		$scope.newItemModal.hide();
		$scope.showLoading('New item created', 1500);
		
		//should be throwing an alert message that the item has been saved...
		//how do we notify the other user in the group that another item was saved
		//do push notifications get called from the server?
		//can I just call out to the service and have one sent?
		//or can I just sync every 5 mins - or on user request?
	};
	
	newItemPostFail = function (result) {
		console.log('NEW ITEM POST::: posting was a FAIL');
		console.log('NEW ITEM POST::: something = ', result);
	};
	
	
	$scope.homeMenuClick = function() {
        console.log('handling a home click from a directive');
		$state.go('home');
	};
	
	$scope.allItemsClick = function() {
		$state.go('itemsList');
	};
	
	$scope.listCategoriesClick = function() {
		$state.go('categoriesList');
	};
	
	$scope.dateClick = function() {
		$state.go('dateMaker');
	};
	
	$scope.openLoveModal = function() {
		console.log('MODALE::: loveModal = ', $scope.loveModal);
		$scope.loveModal.show();
	};
	
	$scope.closeLoveModal = function() {
		$scope.loveModal.hide();
	};

	$scope.openLoginModal = function() {
		console.log('MODALE::: loveModal = ', $scope.loveModal);
		$scope.loginModal.show();
	};
	
	$scope.closeLoginModal = function() {
		$scope.loginModal.hide();
	};

	
	$scope.closeCategory = function() {
		$scope.categoryModal.hide();
	};
	
	$scope.addCategoryClick = function() {
		console.log('add category was clicked');
        console.log('AVAILABLE CATEGORIES::: ', $scope.availableIcons);
        $scope.newItemType = 'category';
		$scope.categoryModal.show();
	};
	
	$scope.addItemClick = function() {
		console.log('add was clicked');
		$scope.newItemModal.show();
        $scope.newItemType = 'todo';
	};
	
	$scope.closeNewItem = function() {
		$scope.newItemModal.hide();
	};
	
	$scope.heartClick = function() {
		//showMainMenu();
		var suggestionBG = $('#mainSuggestionPanel');
		var button1 = $("#mainButton1");
		var button2 = $("#mainButton2");
		var addButton = $("#mainButton3");
		
		var hideElements = [suggestionBG, button1, button2, addButton];
		
		$rootScope.called = 0;
		console.log('scope called initialized!! = ' +$rootScope.called);
		
		TweenMax.staggerTo(hideElements, .5, {alpha:0, onComplete:handleComplete, ease:Power2.easeIn}, .1);
		
		//hideSuggestion();
		hideHeart();
		//hideButton1();
		//hideButton2();
		//hideMainAddButton();
	};
	
	handleComplete = function() {
		console.log('handling complete');
		if ($rootScope.called == 3) {
			//console.log('shoudl be going to the categories list');
			//console.log('state =' ,$state);
			//$location.url('/#/categoriesList/');
			
			$state.go('categoriesList');
			//window.location.href= "#/categoriesList";
			//console.log('should have gone by now...');
			
		}
		else {
			$rootScope.called++;
			console.log('scope called currently = ' +$rootScope.called);	
		}
	};
	
	
	hideHeart = function() {
		//hide the heart
		var heartIcon = document.getElementById("heartIconHolder");
		TweenLite.to(heartIcon, 1, {y:800, alpha:0});
	};
	
}])
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//USER controller starts here
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
.controller('userLoginController', ['$scope',
									'$ionicModal',
									'$state',
									'userAndGroupService', 
									function($scope, $ionicModal, $state, userAndGroupService) {
										
	$scope.loggingInUser = {};										

	$scope.doLogin = function() {
		console.log("USER LOGIN::: user model = ", $scope.loggingInUser);
		
		//check that shit with the userAndGroupService
		userAndGroupService.init(serviceAPILocation);
		userAndGroupService.authenticate($scope.loggingInUser.email, $scope.loggingInUser.password);
	};

	
										
									}])
									
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//CATEGORIES controller starting here
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
.controller('categoriesController', [
									'$scope',
									'$ionicModal',
									'$state',
									'categoryService', 
									'categoryItemService',
									'$ionicHistory',
									'$ionicPopup',
									'$ionicNavBarDelegate',
									function($scope, $ionicModal, $state, categoryService, categoryItemService,$ionicHistory,$ionicPopup,$ionicNavBarDelegate) {
	// http://ionicons.com/
	//handle menu clicks
//	$scope.homeMenuClick = function() {
//        console.log('this is a click fromt the categories controller');
//		$state.go('home');
//	};
	
	//alert('categories controller');
	
	$scope.getRandomSuggestion = function(categoryID) {
		console.log('RANDOM SUGGESTION::::: categoryID = ' ,categoryID);
		return categoryItemService.getRandomCategoryItem(categoryID);
	};
	
	$scope.goToCategory = function(categoryId) {
		console.log('goin to the following category: ' +categoryId);
		//console.log('this should give us just a couple more milliseconds');
		$state.go('itemsListByCategory', {categoryID: categoryId});
	};
	console.log('Categorys::: fetching a list of all the categories');
	$scope.categories = categoryService.getAllCategories();
    console.log('returned categories fromt the category service = ', $scope.categories);
    if($scope.categories.length == 0) {
		$ionicPopup.alert({
		 title: 'Alert',
		  content: 'Categories not found!'
							  
		}).then(function(res) {
		 $state.go('home');
		});
	}
	$scope.items = categoryItemService.getAllItems();
  
  $scope.categoryRows = [];
  
 console.log('categories list hello');
										
  var categoryDisplayOptions = ['categoryColumnSuggestion', 'categoryColumnImage', 'categoryColumnSolid'];
  var colWidths = [25, 50, 75, 100];
  
  var i = 0;
  var currentRowNum = 0;

  console.log('preparing to create rows');
  
  while (i < $scope.categories.length) {
	console.log('STARTING::::::::::::::::::::::::::::::::::::::::::::::::::');
	console.log('CATEGORY IS CURRENTLY: ', $scope.categories[i]);
	console.log('I IS CURRENTLY: ', i);
	
  	
  	console.log('$scope.categoryRows[currentRowNum] = ' +$scope.categoryRows[currentRowNum]);
  	if (typeof $scope.categoryRows[currentRowNum] == 'undefined') {
  		
  		console.log('this row is undefined, creating now');
  		//start the array for the current row 
  		$scope.categoryRows[currentRowNum]=[];
  		totalRowWidth = 0;
  	}
  	
  	//create the category object
  	var thisCategoryInfo = {
  							category:$scope.categories[i],
  							displayType:'',
  							displayWidth: 0
  						};
	
	$scope.categoryRows[currentRowNum].push(thisCategoryInfo);
  	
  	var colSizes = [25, 50, 75, 100];
  	
  	var colCount = 0;
  	var currentRowWidth = 0;
  	
  	while(colCount < $scope.categoryRows[currentRowNum].length) {
  		console.log('COUNTING_LENGTH::::: currentRowNum = ', currentRowNum);
  		console.log('COUNTING_LENGTH::::: $scope.categoryRows[currentRowNum].length = ', $scope.categoryRows[currentRowNum].length);
  		console.log('COUNTING_LENGTH::::: columnCount = ', colCount);
  		console.log('COUNTING_LENGTH::::: display width = '+ $scope.categoryRows[currentRowNum][colCount].displayWidth);
  		currentRowWidth += $scope.categoryRows[currentRowNum][colCount].displayWidth;
  		console.log('COUNTING_LENGTH::::: currentRowWidth = ', currentRowWidth);
  		colCount++;
  	}
  	
  	console.log('TOTAL_ROW_WIDTH:::::::::: ' + currentRowWidth);
  	
  	var suggestionIsAnOption = currentRowWidth < 50;
  	console.log('suggestion is an option = ', suggestionIsAnOption);
  	
  	//display rule - suggestion must be 50 or larger
  	if (!suggestionIsAnOption) {
  		//remove suggestion as an option for the display type
  		var categoryDisplayOptionsTemp = angular.copy(categoryDisplayOptions);
  		categoryDisplayOptionsTemp.shift();
  		console.log('NO SUGGESTION OPTION::::: categoryDisplayOptionsTemp = ', categoryDisplayOptionsTemp);
  		
  		var randomTypeNum = Math.floor((Math.random()*categoryDisplayOptionsTemp.length));
  		console.log('NO SUGGESTION OPTION::::: randomTypeNum = ' +randomTypeNum);
  		console.log('NO SUGGESTION OPTION::::: category display option = ' +categoryDisplayOptionsTemp[randomTypeNum]);
  		thisCategoryInfo.displayType = categoryDisplayOptionsTemp[randomTypeNum];
  	}
  	else {
  		var randomTypeNum = Math.floor((Math.random()*categoryDisplayOptions.length));
  		console.log('SUGGESTION::::: randomTypeNum = ' +randomTypeNum);
  		console.log('SUGGESTION::::: category display option = ' +categoryDisplayOptions[randomTypeNum]);
  		thisCategoryInfo.displayType = categoryDisplayOptions[randomTypeNum];	
  	}
  	
  	
  	console.log('DISPLAY TYPE::::: thisCategoryInfo.displayType = ' ,thisCategoryInfo.displayType);
  	
  	//set the background css if this is an image block
  	if (thisCategoryInfo.displayType == 'categoryColumnImage') {
  		thisCategoryInfo.bgCss = {
  									//'background-image': "url('img/category_images/bgs/" +thisCategoryInfo.category.image +"')",
  									'background-image': "url('" +thisCategoryInfo.category.image +"')",
  									'background-repeat': 'cover'
  									};
  									console.log(":::::CATEGORY IMAGE BACKGROUND::::::::" +thisCategoryInfo.category.image);
  	}
  	
  	//if it is a suggestion make sure that we are using a 50, 75 or 100 length 
  	if (thisCategoryInfo.displayType == 'suggestion') {
		
		console.log('SUGGESTION::::: switching on totalRowWidth which is: ', totalRowWidth);
		
	  	switch(totalRowWidth){
	  		case 0:
	  			//remove 25
	  			colSizes = [50, 75, 100];
	  			console.log('SUGGESTION::::: this column can only be 50, 75, or 100 ', colSizes);
	  			break;
	  		case 25:
	  			//remove 100
	  			colSizes = [50, 75];
	  			console.log('SUGGESTION::::: this column can only be 50 or 75 ', colSizes);
	  			break;
	  		case 50:
	  			colSizes = [50];
	  			console.log('SUGGESTION::::: this column can only be 50 ', colSizes);
	  			break;
	  		case 75:
	  			//this is more than likely going to be a problem.
	  			console.log('SUGGESTION::::: ERROR we are in a problem state ', colSizes);
	  			break;
	  	}
  		
  		
  		console.log('SUGGESTION::::: (after switch) colSizes is currently ', colSizes);
  		
  		//no more than 100
  		var randomLengthNum = Math.floor((Math.random()*colSizes.length));
  		console.log('SUGGESTION::::: random number = ' +randomLengthNum);
  		
  		var thisCategoryLength = colSizes[randomLengthNum];
  		console.log('SUGGESTION::::: thisWidth number = ' +thisCategoryLength);
  	}
  	else {
  		
  		switch(totalRowWidth){
	  		case 0:
	  			//can be any size
	  			//colSizes = (50, 75, 100);
	  			console.log('NORMAL::::: this column can be any width ', colSizes);
	  			break;
	  		case 25:
	  			colSizes = [25, 50, 75];
	  			//can be any size
	  			//colSizes = (50, 75);
	  			console.log('NORMAL::::: this column can be 25, 50, 75 ', colSizes);
	  			break;
	  		case 50:
	  			colSizes = [25, 50];
	  			//remove 75
	  			console.log('NORMAL::::: this column can be 25 or 50 ', colSizes);
	  			break;
	  		case 75:
	  			console.log('NORMAL::::: this column can only be 25 ', colSizes);
	  			colSizes = [25];
	  			break;
	  	}
  		
  		
  		console.log('NORMAL::::: (after switch) col sizes = ', colSizes);
  		var randomLengthNum = Math.floor((Math.random()*colSizes.length));
  		console.log('NORMAL::::: random number = ' +randomLengthNum);
  	
  		var thisCategoryLength = colSizes[randomLengthNum]; 
  		console.log('NORMAL::::: this category length = ' + thisCategoryLength);	
  	}
  	
  	thisCategoryInfo.displayWidth = thisCategoryLength;
  	
  	totalRowWidth += thisCategoryLength;
  	console.log('ROWWIDTH::::: this row width = ' + totalRowWidth);
  	
  	//rowLengths[i] = thisCategoryLength;
  	
  	console.log('this row currently contains: ', $scope.categoryRows[currentRowNum]);
  	
  	if (totalRowWidth == 100) {
  		currentRowNum++;
  		console.log('row width = 100, moving to row number ' +currentRowNum);
  	}
  	
  	if (totalRowWidth > 100) {
  		console.log('OMG MY ROW IS WAYYYYY TO BIG: ', totalRowWidth);
  	}
  		
  	i++;
  	
  	//if there are no more columns - fill the last with 
  	if (i == $scope.categories.length) {
  		if(totalRowWidth < 100) {
  			console.log('NoMore&NOT_ENOUGH:::::: ', totalRowWidth);
  			var fillerWidth = 100-totalRowWidth;
  			var filler = {
  							category:'filler',
  							displayType:'filler',
  							displayWidth: fillerWidth
  						};
			$scope.categoryRows[currentRowNum].push(filler);
  		}
  	}
  }
  
  console.log('FINISHED PRODUCT:::::: ', $scope.categoryRows);
  
}])
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//ITEMS controller starting here
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
.controller('todoItemsController',
									['$scope',
									'categoryItemService',
									'categoryService',
									'$stateParams',
									'$state',
									'$ionicPopup',
									'$ionicListDelegate',
									'$ionicModal',
									'$ionicLoading',
									'$http',
									'userAndGroupService',
									function($scope, categoryItemService, categoryService, $stateParams, $state,$ionicPopup,$ionicListDelegate,$ionicModal,$ionicLoading,$http,userAndGroupService) {
	//get all the items for the category
	$scope.sby = '';
	
	$scope.sortCriteria = "";
	$scope.ssort = function (data) {
		$scope.sortCriteria = data;
	};
	console.log('I am in the todoItemsController');
	$scope.msg = {};
	$scope.todoItem = {};
	if ($stateParams.categoryID) {
		console.log('THERE IS A CATEGORY ID PRESENT: ', $stateParams.categoryID);
		$scope.catId = $stateParams.categoryID;
		$scope.category = categoryService.getCategory($stateParams.categoryID);
		console.log('THERE IS A CATEGORY FETCHED: ', $scope.category);
		$scope.items = categoryItemService.getCategoryItems($stateParams.categoryID);
		console.log("=="+JSON.stringify($scope.items));
	}
	else {
		$scope.items = categoryItemService.getAllItems();
		console.log("=="+JSON.stringify($scope.items));
	}
	$ionicModal.fromTemplateUrl('views/newItemModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.newItemModal = modal;
	});
	$ionicModal.fromTemplateUrl('views/messageModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.messageModal = modal;
	});
	$scope.user = userAndGroupService.verifyUser();
	console.log('MAIN CONTROLLER USER::: = ' + JSON.stringify($scope.user));
	 //image capture                                
    $scope.captureImage = function(itemType) {
		
		console.log('CAMERA::::', navigator.camera);
		
		navigator.camera.getPicture(onSuccess, onFail, { quality          : 50,
                                                        destinationType   : Camera.DestinationType.FILE_URI
                                                       });
        
        var passedType = itemType;
        //does camera.getPicture allow you to pass additional info that is going to be passed to the success function
        //if not we need to set a global toggle to inidicate what type of new item we are creating.
        
	
		function onSuccess(imageURI) {
		   
            if(itemType == 'category') {
                console.log('IMAGE CAPTURE SUCCESS:: sucessfully selected a category image');
            }
            
		   //button stays to allow the user to take another photo
		   //or the button is gone
		    
		    //show the user a preview of what they selected
		    //var image = document.getElementById('takenImage');
		    //image.src = imageURI;
		    //make sure that we have this
		    $scope.newItemCapturedImage = imageURI;
		    //$scope.todoItem.imageType = 'taken';
            console.log('the passed item type = ' + passedType);
		    console.log('IMAGE CAPTURE::: image = ' +$scope.newItemCapturedImage);
		}
		
		function onFail(message) {
		    alert('Image Capture Failed. Message: ' + message);
		}

	};
	
	//image selection
	$scope.selectImage = function(itemType) {
		
		console.log('CAMERA::::', navigator.camera);
		
		navigator.camera.getPicture(onSuccess, onFail, { quality		: 50,
														sourceType      : Camera.PictureSourceType.SAVEDPHOTOALBUM,
	    												destinationType	: Camera.DestinationType.FILE_URI
                                                       });
	   
        var passedType = itemType;
        
		function onSuccess(imageURI) {
		   
		   //button stays to allow the user to take another photo
		   //or the button is gone
		    
		    //show the user a preview of what they selected
		    //var image = document.getElementById('takenImage');
		    //image.src = imageURI;
		    //make sure that we have this
		    $scope.newItemCapturedImage = imageURI;
		    //$scope.todoItem.imageType = 'selected';
		    
		    console.log('IMAGE SELECT::: image = ' +$scope.newItemCapturedImage);
		}
		
		function onFail(message) {
		    //alert('Failed because: ' + message);
		}

	};
	$scope.showLoading = function(loaderText, loadDuration) {
		if (loadDuration === 'undefined') { loadDuration = 0; }
		$ionicLoading.show({
			//templateUrl: 'views/loaderTemplate.html',
			template: "<ion-spinner icon='ripple'></ion-spinner>" +loaderText,
			duration: loadDuration
		});
	};
	
	
	$scope.hideLoading = function() {
		$ionicLoading.hide();
	};
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	// form handling for the creation of new items
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////

	$scope.err = function(err1) {
			
			$ionicPopup.alert({
			 title: 'Alert..!',
			 template: 'Please fill '+"'"+err1+"'!"
			}).then(function(res) {
			 
			});
			
		};
	$scope.postNewItemDetails = function() {
		console.log("validating");
		
		
		if($scope.todoItem.title === undefined) {
			$scope.err('What are we doing');
		} else if($scope.todoItem.location === undefined) {
			$scope.err('Location');
		} else if($scope.todoItem.cost === undefined) {
			$scope.err('Cost');
		} else if($scope.todoItem.startDay === null) {
			$scope.err('StartDate');
		} else if($scope.todoItem.startTime === null) {
			$scope.err('StartTime');
		} else if($scope.todoItem.endDay === null) {
			$scope.err('EndDate');
		} else if($scope.todoItem.endTime === null) {
			$scope.err('EndTime');
		} else {
        $scope.showLoading('Updating a new entry','2000');
		
		//working off this:
		//http://metabates.com/2012/04/15/capturing-and-uploading-photos-on-ios-with-phonegap/
		
		console.log('UPDATE ITEM POST::: $scope.newItemCapturedImage = ', $scope.newItemCapturedImage); 
		//$scope.showLoading('Creating a new item');
		
		  if ($scope.newItemCapturedImage != undefined) {
			console.log('UPDATE ITEM POST::: todoItem model = ', $scope.todoItem);
              //requires: 
            //cordova plugin add org.apache.cordova.file-transfer
            //https://github.com/apache/cordova-plugin-file-transfer/blob/master/doc/index.md
            options = new FileUploadOptions();

            // parameter name of file:
            options.fileKey = 'newItemFile';

            // name of the file:
            options.fileName = $scope.newItemCapturedImage.substr($scope.newItemCapturedImage.lastIndexOf('/') + 1);  

            // mime type:
            //options.mimeType = "text/plain";  
            
            $scope.todoItem.userGroup = $scope.user.groupId;  
              
            params = $scope.todoItem;

            options.params = params;
            ft = new FileTransfer();
            
            //this should also point to the local server
              
            
            //this should be more generic
            //was: http://romantica.io/api/toDoItems.php?action=create
            if($scope.newItemType == 'todo') {
                
                if (serviceAPILocation == 'local') {
                    postUrl = 'http://127.0.0.1/romantisca/api/toDoItems.php?action=create';
                }
                else {
                    postUrl = 'http://romantica.io/api/toDoItems.php?action=create';
                }
                
                ft.upload($scope.newItemCapturedImage, postUrl, $scope.newItemPostSuccess, newItemPostFail, options);
            } else if ($scope.newItemType == 'category') {
                ft.upload($scope.newItemCapturedImage, postUrl, $scope.newItemPostSuccess, newItemPostFail, options);
            }
            
              
	       }
		  else {
		  	console.log("Update ITEM POST::: no new image added");
            
            $scope.todoItem.DEFAULT_IMAGE = 'DEFAULT'
            
            if (serviceAPILocation == 'local') {
                serverPrefix = 'http://127.0.0.1/romantisca/api/';
            }
              
                else {
                serverPrefix = 'http://romantica.io/api/';
            }
            
            //based on switch set by modal opening
            //default is todo item
            if($scope.newItemType == 'todo') {
                createUrl =  serverPrefix + 'toDoItems.php?action=update&id='+$scope.toDoItem.id,
                params = $scope.todoItem;
                paramData = $.param($scope.todoItem);
                //fill in with data property below
            } else if ($scope.newItemType == 'category') {
                createUrl = serverPrefix +'categories.php?action=update&id='+$scope.toDoItem.id,
                params = $scope.newCategory;
                paramData = $.param($scope.newCategory);
            }
              
            //post the form using the http method
            $http({
                method: 'POST',
                //this should be more generic
                //was: http://romantica.io/api/toDoItems.php?action=create
                url: createUrl,
                data: paramData,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
            .success(function(data){
                console.log(data);
                
                if (data.result != 'yes') {
					console.log('data = ', data);
					console.log('data.result = ', data.result);
                    //if not successful, bind errors to scope elements
                    //todo llater
                    $scope.hideLoading();
                    $scope.newItemModal.hide();
                    $scope.showLoading('There was an error updating item', 1500);
                    
                }
                else {
                    //handle the success of the post
                    $scope.hideLoading();
                    $scope.newItemModal.hide();
                    $scope.showLoading('item updated', 1500);
                }
            });
		  }
		}
	};
	
	$scope.newItemPostSuccess = function (result) {
		console.log('UPDATE ITEM POST SUCCESS::: posting was a success');
		console.log('UPDATE ITEM POST SUCCESS::: something = ', result);
		console.log('UPDATE ITEM POST SUCCESS::: newItemModal = ', $scope.newItemModal);
        
        ////////////////////////////////////////////////////////////////////
        // testing this - want to be able to see the new items as they are posted 
        // without shutting down the app anc coming back in
        ////////////////////////////////////////////////////////////////////
        console.log('UPDATE ITEM POST SUCCESS::: calling reload on the category items = ');
        //categoryService.reloadItems();
		
        
        $scope.hideLoading();
		$scope.newItemModal.hide();
		$scope.showLoading('Item updated', 1500);
		
		//should be throwing an alert message that the item has been saved...
		//how do we notify the other user in the group that another item was saved
		//do push notifications get called from the server?
		//can I just call out to the service and have one sent?
		//or can I just sync every 5 mins - or on user request?
	};
	
	newItemPostFail = function (result) {
		console.log('NEW ITEM POST::: posting was a FAIL');
		console.log('NEW ITEM POST::: something = ', result);
	};
	$scope.closeNewItem = function() {
		$scope.newItemModal.hide();
	};
	$scope.closeMessageModal = function() {
		$scope.messageModal.hide();
	};
	//handle menu clicks
	$scope.homeMenuClick = function() {
        console.log('this is a click frokt the toDo iems controller');
		$state.go('home');
	};
	
	console.log('I have items: ', $scope.items);
	
	//on item click go to todoItem detail
	$scope.handleItemClick = function(incomingItemID) {
		console.log('handling an item click on item:', incomingItemID);
		$state.go('itemDetail', {itemID:incomingItemID});
	};
	
	//handle button clicks:
	$scope.handleDeleteClick = function(itemId,item) {
		console.log('handling a delete click on item:', itemId);
		//make sure
		var confirmPopup = $ionicPopup.confirm({
		 title: 'Delete!',
		 template: 'Are you sure you want to delete this todo?'
		}).then(function(res) {
		 if(res) {
			 console.log("deleted");
		   $scope.items.splice($scope.items.indexOf(item), 1);
		   categoryItemService.deleteItem(itemId);
		 } else {
		   console.log("no");
		   $ionicListDelegate.closeOptionButtons();
		 }
	    });
		
		
		
	};
	
	$scope.handleEditClick = function(itemId) {
		
		console.log('handling an edit click on item:', itemId);
		$ionicListDelegate.closeOptionButtons();
		var confirmPopup = $ionicPopup.confirm({
		 title: 'Edit!',
		 template: 'Are you sure you want to Edit this todo?'
		}).then(function(res) {
		 if(res) {
			$scope.thisitems = categoryItemService.getItem(itemId);
		console.log("=="+JSON.stringify($scope.thisitems));
		$scope.newItemModal.show();
        $scope.newItemType = 'todo';
		$scope.todoItem.id= $scope.thisitems.id;
		$scope.todoItem.title= $scope.thisitems.title;
		$scope.todoItem.desc= $scope.thisitems.description;
		$scope.todoItem.location= $scope.thisitems.location;
		$scope.todoItem.cost= $scope.thisitems.cost;
		$scope.todoItem.startDay= new Date($scope.thisitems.start_date);
		$scope.todoItem.startTime= new Date($scope.thisitems.start_date);
		$scope.todoItem.endDay= new Date($scope.thisitems.end_date);
		$scope.todoItem.endTime= new Date($scope.thisitems.end_date);
		 } else {
		   console.log("no");
		   $ionicListDelegate.closeOptionButtons();
		 }
	    });
		
		
		
	};
	$scope.doMessage = function() {
		$ionicListDelegate.closeOptionButtons();
		if($scope.msg.subject=== undefined) {
			$scope.err('Message');
		} else {
			
			$scope.showLoading('Sending message..',1500);
			
			$scope.msg.group = $scope.user.groupId;
			$scope.msg.from = $scope.user.id;
			if (serviceAPILocation == 'local') {
                serverPrefix = 'http://127.0.0.1/romantisca/api/';
            }
              
                else {
                serverPrefix = 'http://romantica.io/api/';
            }
			createUrl =  serverPrefix + 'messages.php?action=create';
                params = $scope.msg;
                paramData = $.param($scope.msg);
                //fill in with data property below
            
            //post the form using the http method
            $http({
                method: 'POST',
                //this should be more generic
                //was: http://romantica.io/api/toDoItems.php?action=create
                url: createUrl,
                data: paramData,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
            .success(function(data){
                console.log(data);
                
                
					console.log('data = ', data);
					
                    $scope.hideLoading();
                    $scope.messageModal.hide();
                    /*$ionicPopup.alert({
					 title: 'Success..!',
					 template: 'Submited successfully'
					}).then(function(res) {
					 
					});*/
                    
               
            });
			$scope.hideLoading();
            $scope.messageModal.hide();
		}
		
	};
	$scope.handleMessageClick  = function(itemId) {
		console.log('handling a message click on item:', itemId);
		$ionicListDelegate.closeOptionButtons();
		$scope.messageModal.show();
	};
	
	$scope.handleLikeClick = function(itemId) {
		console.log('handling a like click on item:', itemId);
		$scope.like = {};
		
		$scope.likedBy = $scope.user.first_name.substring(0,5);;
			
			categoryItemService.likeit(itemId,$scope.likedBy);
            
					$ionicListDelegate.closeOptionButtons();
                    $ionicPopup.alert({
					 title: 'Success..!',
					 template: 'Successfully'
					}).then(function(res) {
					 
					});
                    
               
            
	};
	
	$scope.handleLetsDoItClick = function(whichItem) {
		console.log('handling a DO IT click for the item ' +whichItem);
		
	};
	$scope.sortingby = function() {
		$ionicPopup.show({
                    title: 'SortBy',
                    templateUrl: 'views/sort.html',
                    scope: $scope,
                    buttons: [
                        {text: 'Cancel'},
                        {
                            text: 'Sort',
                            type: 'button-positive',
                            onTap: function (e) {
                                console.log($scope.sortCriteria);
								$scope.sby = $scope.sortCriteria;
                            }
                        },
                    ]

                });
	};
}])
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//ITEM detail controller starting here
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
.controller('itemDetailController', ['$scope', 'categoryItemService', '$stateParams', function($scope, categoryItemService, $stateParams) {
	//alert('item detail');
	console.log('we are in the item detail controller',$stateParams.itemID);
	
	//get the item details fromt the service
	//set thme on the scope
	categoryItemService.init(serviceAPILocation)
	.then(function(data) {
		if (data) {
			$scope.item = categoryItemService.getItem($stateParams.itemID);
			console.log('CATEGORY ITEM SERVICE (fetching item)::: the item has been fetched:', $scope.item);
			console.log('ITEM DETAIL::: the item is done = ' +$scope.item.done);
			
			//change to true or false for done
			if ($scope.item.done == 0) {
				$scope.item.doneBool = false;
			}
			else {
				$scope.item.doneBool = true;
			}
			console.log('ITEM DETAIL::: the item done bool = ' +$scope.item.doneBool);
		}
	});
	
	//var startDate = moment($scope.item.validDates[0]).format("MMM Do");
	//var endDate = moment($scope.item.validDates[1]).format("MMM Do");
	
	
	//$scope.item.startDate = startDate;
	//$scope.item.endDate = endDate;
	
	
	//handle the edit click
	//handle the delete click
	//handle the message lists
	//handle to the 'Done Click' (there has to be something here)
	//I have a notion of being able to
		//a:::) suggest that something be done (lets do it)
		//b:::) [SAVE FOR LATER] create a date, that is a date/time with attached todo items stored as it's own entry and sent as a suggestion to the other member (make a date)
		//c:::) mark a todo item as done (mark as complete)
		
	//handle button clicks
	$scope.handleDoneClick = function(whichItem){
		
		//should later popup a message with a 'suggest this to other users?' which will make this publicly available as a todo
		$scope.item.doneBool = true;
		$scope.item.done = 1;
		var centerImage = $('#circular-center');
		var centerHeart = $('#circular-heart');
		//centerImage.fadeTo('fast', .5);
		centerHeart.fadeIn('fast');
		categoryItemService.markItemDone(whichItem);
		//show the heart icon in the red circle outline
		//change the color of the icon
		
		
		console.log('an item should be marked as done: ', whichItem);
		//color the icon to red or purp
		//update the dB with done = true for that item
		
	};
	
	handleDetailDoneFadeComplete = function() {
		
		console.log('::::::::::::::::::::::::::::::::::::::::::::::');
		console.log('the animation should be marked as complete now');
		console.log('::::::::::::::::::::::::::::::::::::::::::::::');
	};
		
}]);
